## Development and Change Guidelines

This document describes the guidelines for a transparent and open mechanism for deciding how to evolve the ONVIF specifications. 
The ONVIF Technical Committee implements the process described below when merging changes from internal or external contributors. 

## Specification Change Categories

Changes should fall into the following three categories:

* Clarification. The current description either doesn't make sense, is ambiguous, overly complicated, or unclear.

* Consistency. A portion of the specification is not consistent with the rest, or contraditcs to underlying specifications.

* New functionality. The proposal adds one or more features to the specifications.

## Specification Change Process

The change process depends on both the size and impact of the change proposal: 

Each change request (CR) shall be submitted as merge request. The comment for a merge request shall include the following information:
* Reason: containing a problem description and a description of the solution proposal.
* Compatibility analysis: An analysis describing possible impact on forward and or backward compatibility. 

For larger additions affecting more than a simple type please see section "Feature Development Process" below.

## Specification workflow

* ONVIF shall maintain the following active branches:

    * master - Current stable version. No CRs are accepted directly to modify the specification.

    * dev-xx.nn - Development branch for next version

    * dev-xx.oo - Development branch for version after next version

    * exp-xx.yy - Closed development branch pending for IPR review. Implementers may use this branch. 
	  However in the very unlikely case of IPR notices indiviual changes may be revoked.

## Feature Development Process

* New features shall not be developed on development branches but in separate feature branches. 
Feature branches may be part of the main ONVIF specification repository, but are typically located in separate repositories.

* Once an author or group has completed the specification development for a feature the respective feature needs to be verified by writing and executing test cases.
* When all activities have been completed the merge request shall include the following information:
  * Reason: containing a problem description and a description of the solution proposal.
  * Compatibility analysis: An analysis describing possible impact on forward and or backward compatibility. 
  * Unit tests covering the additional features
  * Information which parties implemented client and server side as well as a test report from at least two different entities.

## Participation

While governance of the specification is the role of the TC, the evolution of the specification happens through the participation of members of the developer community at large. 
Any person willing to contribute to the effort is welcome, and contributions may include filing or participating in issues, creating pull requests, or helping others with such activities.

